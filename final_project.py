"""
author: Haohang Li
date: 3/13/2021
section leader: Ales Waskow 001B
ISTA131 final project
Summary of the module: This module is the program for the final project. We collected data from
https://data.worldbank.org/ for population for different countries vs year; older people (>= 65) for China, as well as
gender ratio for different countries. Then, we plotted and saved the figures for the 3 collection of data described
above.
"""

import numpy as np
import pandas as pd
from scipy.stats import linregress
import matplotlib.pyplot as plt
import seaborn as sns


def get_ols_parameters(s):
    """
    This function takes a Series, fits a line to it, and returns the slope, intercept, R2, and the p-value in a list.
    :param s: a series of the data
    :return: a list contains the slope, intercept, r value ** 2, and p value
    """
    s.index = s.index.astype(int)

    regression_model = linregress(s.index, s.values)

    model_info = [
        regression_model.slope,
        regression_model.intercept,
        regression_model.rvalue ** 2,
        regression_model.pvalue
    ]

    return model_info


def plot_older_scatter():
    """
    Read the data from China.csv which includes the population for the old people (>=65) in China from 1960 to 2019.
    Then, generate a figure as well as the exponential model prediction using linear regression for the logarithm of
    the original data.
    Note: you need to call plt.show() to show the plot.
    :return: None
    """
    df_China = pd.read_csv(filepath_or_buffer='China.csv',
                           index_col=0,
                           skiprows=4,
                           usecols=[2] + list(range(4, 64)))

    # plot old people line
    older_people = df_China.loc['Population ages 65 and above (% of total population)']
    older_people.index = older_people.index.astype(int)

    plt.scatter(x=older_people.index,
                y=older_people,
                label="older people real data",
                s=10,
                color='r')

    # log(percentage)
    older_people = np.log(older_people)
    plt.scatter(x=older_people.index,
                y=older_people,
                label="logarithm for real data",
                s=10,
                color='b')

    year_list = list(range(1960, 2051))

    # get the linear regression model.
    regression_model = get_ols_parameters(older_people)
    slope = regression_model[0]
    intercept = regression_model[1]

    # create the year to year regression model.
    regression_older = []
    for year in year_list:
        regression_older.append(slope * year + intercept)

    # convert the list as a series so that we can plot it.
    regression_older = pd.Series(data=regression_older, index=year_list)
    regression_older.plot(label='regression for log(%population)')

    np.exp(regression_older).plot(label=r'exponential of regression value $(e^{x})$')

    plt.legend()
    plt.xlim(1960, 2050)
    plt.xlabel('year', fontsize=18)
    plt.title(r'People $\geq$ 65 years old in China', fontsize=18)
    plt.ylabel('total population (percentage)', fontsize=20)

    return


def plot_total():
    """
    This function reads the data from csv file and plots the total population for India, China, the United States, and
    Japan. It also did the linear regression for the population and plotted the data.
    Note: you need to call plt.show() to show the plot.
    :return: None
    """
    df = pd.read_csv(filepath_or_buffer='population_total.csv',
                     index_col=0,
                     skiprows=4,
                     usecols=[0] + list(range(4, 64)))
    df.columns = df.columns.astype(int)

    s_China = df.loc['China',] / 1e9
    s_Japan = df.loc['Japan',] / 1e9
    s_India = df.loc['India',] / 1e9
    s_UnitS = df.loc['United States',] / 1e9

    plt.scatter(x=s_China.index.astype(int), y=s_China, label='China', s=5)
    plt.scatter(x=s_Japan.index.astype(int), y=s_Japan, label='Japan', s=5)
    plt.scatter(x=s_India.index.astype(int), y=s_India, label='India', s=5)
    plt.scatter(x=s_UnitS.index.astype(int), y=s_UnitS, label='United States', s=5)

    year_list = list(range(2000, 2051))

    # get the linear regression model.
    [slope_China, intercept_China, _, _] = get_ols_parameters(s_China.loc[2000:])
    [slope_Japan, intercept_Japan, _, _] = get_ols_parameters(s_Japan.loc[2000:])
    [slope_India, intercept_India, _, _] = get_ols_parameters(s_India.loc[2000:])
    [slope_UnitS, intercept_UnitS, _, _] = get_ols_parameters(s_UnitS.loc[2000:])

    # create the year to year regression model.
    regression_China = [slope_China * year + intercept_China for year in year_list]
    regression_Japan = [slope_Japan * year + intercept_Japan for year in year_list]
    regression_India = [slope_India * year + intercept_India for year in year_list]
    regression_UnitS = [slope_UnitS * year + intercept_UnitS for year in year_list]

    # convert the list as a series so that we can plot it.
    pd.Series(data=regression_China, index=year_list).plot(label='China regression')
    pd.Series(data=regression_Japan, index=year_list).plot(label='Japan regression')
    pd.Series(data=regression_India, index=year_list).plot(label='India regression')
    pd.Series(data=regression_UnitS, index=year_list).plot(label='United States regression')

    plt.legend(loc=2)
    plt.xlim(1960, 2051)
    plt.xlabel('year', fontsize=18)
    plt.title(r'Population for Different Countries', fontsize=18)
    plt.ylabel('population (billion)', fontsize=20)

    return


def gender_ratio_boxplot():
    """
    This function plots the boxplots of gender ratio for India, China, the United States, and Japan.
    :return:
    """
    df = pd.read_csv(filepath_or_buffer='gender_ratio.csv',
                     index_col=0,
                     skiprows=4,
                     usecols=[0] + list(range(4, 64)))
    df = df.astype(float)

    # the original data is the count of female population per 100 total population. Below formula is to convert it to
    # the percentage of female / male.
    df = df / 50 * 100

    s_China = df.loc['China',]
    s_Japan = df.loc['Japan',]
    s_India = df.loc['India',]
    s_UnitS = df.loc['United States',]

    data = {
        'India': s_India,
        'China': s_China,
        'United States': s_UnitS,
        'Japan': s_Japan,
    }

    df = pd.DataFrame(index=s_China.index, data=data)

    # boxplot
    sns.boxplot(x="variable", y="value", data=pd.melt(df), width=0.5)

    plt.title(r'Gender Ratio: $\frac{female}{male}\times 100\%$', fontsize=18)
    plt.xlabel('Countries', fontsize=18)
    plt.xticks(fontsize=12)
    plt.ylabel(r'gender ratio (percentage)', fontsize=17)
    plt.tight_layout()
    plt.show(block=False)

    return


def main():
    """
    This function calls the functions to plot the figures and save the figures.
    :return:
    """
    plot_total()
    plt.show(block=False)
    # plt.savefig('population.png')

    plt.figure()
    plot_older_scatter()
    plt.show(block=False)
    # plt.savefig('China_older.png')

    plt.figure()
    gender_ratio_boxplot()
    plt.show()
    # plt.savefig('gender_ratio.png')


if __name__ == '__main__':
    main()
